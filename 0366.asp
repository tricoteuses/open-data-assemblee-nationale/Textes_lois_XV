<?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"><style type="text/css"> { font-family:'Times New Roman'; font-size:14pt; text-align:justify }h1, h2, h3, h4, h5, h6, p { margin:0pt }li { margin-bottom:0pt; margin-top:0pt }h1 { color:#ff00ff; font-size:14pt; font-weight:bold; margin-top:12pt; text-align:center; text-transform:uppercase }h2 { color:#ff00ff; font-size:14pt; font-weight:bold; text-align:center; text-transform:uppercase }h3 { color:#008000; font-size:14pt; font-weight:bold; margin-left:28.35pt; text-align:justify; text-indent:-28.35pt; text-transform:uppercase }h4 { font-size:10pt; font-weight:bold; margin-left:85.05pt; text-align:justify; text-indent:-21.3pt; text-transform:uppercase }h5 { font-size:14pt; font-weight:bold; margin-left:85.05pt; text-align:justify; text-indent:-21.3pt }h6 { font-size:14pt; font-style:italic; font-weight:normal; margin-left:85.05pt; text-align:justify; text-indent:-21.3pt }.aHeading7 { font-size:14pt; font-style:italic; font-weight:bold; margin-left:85.05pt; text-align:justify; text-indent:-21.3pt }.aHeading8 { font-size:14pt; font-weight:bold; margin-left:85.05pt; text-align:justify; text-indent:21.3pt }.aHeading9 { font-size:10pt; font-style:italic; font-weight:normal; margin-left:85.05pt; text-align:justify; text-indent:21.3pt }.a1TomeIntit0 { font-family:Arial; font-size:13pt; font-style:italic; font-weight:bold; page-break-after:avoid; text-align:center }.a1TomeNum0 { font-family:Arial; font-size:13pt; font-style:italic; margin-top:12pt; page-break-after:avoid; text-align:center }.a2PartieIntit0 { font-family:'Arial Narrow'; font-size:13pt; font-style:italic; font-weight:bold; page-break-after:avoid; text-align:center }.a3LivreNum { font-size:13pt; font-style:italic; margin-bottom:12pt; text-align:center; text-indent:0pt }.a4TitreIntit0 { font-size:14pt; font-style:italic; font-weight:bold; margin-bottom:30pt; margin-top:0pt; page-break-after:avoid; text-align:center; text-indent:0pt; text-transform:uppercase }.a4TitreNum0 { color:#000000; font-size:14pt; font-style:italic; margin-top:12pt; text-align:center; text-transform:uppercase }.a5ChapitreIntit0 { font-size:14pt; font-style:italic; font-weight:bold; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a5ChapitreNum { font-size:14pt; font-style:italic; font-variant:small-caps; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a6SectionIntit0 { font-size:14pt; font-style:italic; font-weight:bold; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a6SectionNum0 { font-size:14pt; font-style:italic; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a7Sous-sectionIntit0 { font-size:14pt; font-style:italic; font-weight:normal; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a7Sous-sectionNum { font-size:14pt; font-style:italic; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a8ParagrapheIntit { font-size:14pt; font-style:italic; font-weight:normal; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a8ParagrapheNum { font-size:14pt; font-style:italic; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a9ArticleNum0 { font-size:14pt; font-style:italic; font-weight:bold; margin-bottom:2pt; margin-top:15pt; page-break-after:avoid; text-align:center; text-indent:0pt }.aLoiTexte { font-size:14pt; margin-bottom:12pt; text-align:justify; text-indent:25.5pt }.a1TomeIntit { font-family:Arial; font-size:13pt; font-weight:bold; page-break-after:avoid; text-align:center }.a1TomeNum { font-family:Arial; font-size:13pt; margin-top:12pt; page-break-after:avoid; text-align:center }.a2PartieIntit { font-family:'Arial Narrow'; font-size:13pt; font-weight:bold; page-break-after:avoid; text-align:center }.a2PartieNum { font-family:Arial; font-size:13pt; margin-top:12pt; page-break-after:avoid; text-align:center }.a3LivreIntit { font-family:'Arial Narrow'; font-size:13pt; font-weight:bold; page-break-after:avoid; text-align:center }.a3LivreNum0 { font-family:'Times New Roman'; font-size:13pt; margin-top:12pt; page-break-after:avoid; text-align:center }.a4TitreIntit { font-size:14pt; font-weight:bold; margin-bottom:30pt; margin-top:0pt; page-break-after:avoid; text-align:center; text-indent:0pt; text-transform:uppercase }.a4TitreNum { font-size:14pt; margin-bottom:12pt; margin-top:24pt; page-break-after:avoid; text-align:center; text-indent:0pt; text-transform:uppercase }.a4titreintit1 { font-family:'CG TIMES (WN)'; font-size:14pt; font-weight:bold; margin-bottom:12pt; margin-top:3pt; text-align:center; text-transform:uppercase }.a5ChapitreIntit { font-size:14pt; font-weight:bold; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a5ChapitreNum0 { font-size:14pt; font-variant:small-caps; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a6SectionIntit { font-size:14pt; font-weight:bold; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a6SectionNum { font-size:14pt; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a7Sous-sectionIntit { font-size:14pt; font-weight:normal; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a7Sous-sectionNum0 { font-size:14pt; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a8ParagrapheIntit0 { font-size:14pt; font-weight:normal; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a8ParagrapheNum0 { font-size:14pt; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a9ArticleNum { font-size:14pt; font-weight:bold; margin-bottom:12pt; margin-top:24pt; page-break-after:avoid; text-align:center; text-indent:0pt }.Textenonpastill { font-size:14pt; margin-bottom:12pt; text-align:justify; text-indent:25.5pt }.aTitrePG { font-family:'Times New Roman Italique'; font-size:14pt; letter-spacing:0.75pt; margin-bottom:6pt; margin-top:6pt; text-align:center }.aBalloonText { font-family:Tahoma; font-size:8pt; text-align:justify }.aBodyTextIndent { font-size:14pt; text-align:justify; text-indent:27pt }.aCaption { font-size:24pt; font-weight:bold; margin-top:126pt; text-align:center; text-transform:uppercase }.aDate { font-size:14pt; text-align:justify }.Dlibr { font-size:14pt; font-style:italic; margin-bottom:12pt; margin-top:24pt; text-align:justify; text-indent:25.5pt }.aFooter { font-size:14pt; text-align:justify }.aFootnoteText { font-size:14pt; text-align:justify; text-indent:25.5pt }.aHeader { font-size:11pt; margin-bottom:12pt; text-align:center }.aNormalWeb { font-size:14pt; margin-bottom:5pt; margin-top:5pt; text-align:left }.aProjet { font-size:14pt; font-style:italic; margin-bottom:0pt; margin-left:-7.1pt; margin-right:-26.7pt; text-align:center }.Prsident { font-size:14pt; font-style:italic; margin-top:18pt; text-align:justify }.SignaturePrsident { font-size:14pt; font-style:italic; margin-top:6pt; text-align:justify }.aTITRE { font-size:14pt; margin-bottom:12pt; margin-top:24pt; text-align:center; text-transform:uppercase }.aTITRE4 { font-family:'CG TIMES (WN)'; font-size:12pt; font-weight:normal; margin-bottom:12pt; margin-left:-7.1pt; margin-right:-26.7pt; text-align:justify; text-indent:-21.3pt; text-transform:uppercase }.aTitle { font-size:14pt; margin-bottom:12pt; margin-left:-7.1pt; margin-right:-26.7pt; text-align:center }.aTitre1 { font-size:18pt; font-weight:bold; margin:24pt -26.7pt 24pt -7.1pt; text-align:center }.aTitre7 { font-size:18pt; font-weight:bold; margin:24pt -26.7pt 24pt -7.1pt; text-align:justify }.aTitre8 { font-size:14pt; margin-bottom:12pt; margin-left:106.35pt; margin-right:93.75pt; text-align:center }.Titre0 { font-size:14pt; margin-bottom:12pt; margin-left:-7.1pt; margin-right:-26.7pt; text-align:center; text-decoration:underline }.aTrait { border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:1.5pt; font-size:14pt; margin-left:-7.1pt; margin-right:-26.7pt; padding-bottom:1pt; text-align:center }.aconstitution { font-size:14pt; letter-spacing:0.75pt; margin-top:6pt; text-align:center }.adroite { font-size:14pt; text-align:right }.enregistr { border-top-color:#000000; border-top-style:double; border-top-width:1.5pt; font-size:11pt; margin-left:-21.3pt; margin-right:-21.3pt; margin-top:6pt; padding-top:5pt; text-align:center }.atreizieme { font-size:10pt; letter-spacing:0.75pt; margin-top:6pt; text-align:center }span.aLoiTexteCar { font-size:14pt }span.aEloiPastille {white-space: normal;float:left;clear:left; font-size:18pt; font-weight:bold }span.aLnom { font-family:'Times New Roman'; font-size:13pt; text-decoration:none; text-transform:uppercase; vertical-align:baseline }span.Lprnom { font-family:'Times New Roman'; font-size:13pt; text-decoration:none; vertical-align:baseline }span.aPastille { color:#000000; font-size:12pt; font-weight:bold; text-decoration:none; vertical-align:baseline }span.aPastilleBis { color:#000000; font-size:12pt; font-weight:bold }span.aEmphasis { font-style:italic }span.aFootnoteReference { vertical-align:super }span.aPageNumber { font-family:'Times New Roman' }span.aPlaceholderText { color:#808080 }span.aStrong { font-weight:bold }span.aStyle24ptToutenmajuscule { font-family:'Times New Roman'; font-size:24pt; text-transform:uppercase }  p{margin-left:40px;} td p{margin-left:0px;} p.StylePastille{margin-left:0px;}  </style><div xmlns="http://www.w3.org/1999/xhtml" ><head><title>PROJET DE LOI</title></div><?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"><!--[if gte mso 9]><xml></div><?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"><o:DocumentProperties><o:Title>PROJET DE LOI</o:Title><o:Author>Évelyne Baka-Achard</o:Author><o:LastAuthor>Évelyne Baka</o:LastAuthor><o:Revision>3</o:Revision><o:TotalTime>1</o:TotalTime><o:LastPrinted>2015-06-08T12:58:00Z</o:LastPrinted><o:Created>2017-11-16T13:49:00Z</o:Created><o:LastSaved>2017-11-16T13:49:00Z</o:LastSaved><o:Pages>2</o:Pages><o:Words>344</o:Words><o:Characters>2102</o:Characters><o:Company>SENAT</o:Company><o:Lines>17</o:Lines><o:Paragraphs>4</o:Paragraphs><o:CharactersWithSpaces>2442</o:CharactersWithSpaces><o:Version>14.0000</o:Version></o:DocumentProperties></div><?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"></xml><![endif]--></div><?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"></head><body><div><p style="text-align:center"><img src="0366images/temporaryFile.001.png" width="144" height="82" alt="Description : LOGO" /></p><p style="text-align:center"><span style="font-size:28pt">N</span><span style="font-size:28pt; vertical-align:3pt">°</span><span style="font-size:28pt">&#xa0;</span><span style="font-size:28pt">366</span></p><p style="text-align:center"><span style="font-weight:bold; vertical-align:3pt">_____</span></p><p style="text-align:center"><span class="aStyle24ptToutenmajuscule" style="font-size:28pt">ASSEMBLÉE  NATIONALE</span></p><p class="aconstitution"><span style="letter-spacing:0pt">CONSTITUTION DU 4 OCTOBRE 1958</span></p><p class="atreizieme"><span style="letter-spacing:0pt">QUINZIÈME</span><span style="letter-spacing:0pt"> </span><span style="letter-spacing:0pt">LÉGISLATURE</span></p><p class="enregistr" style="margin-left:0pt; margin-right:0pt"><span>Enregistré à la Présidence de l</span><span>’</span><span>Assemblée nationale le </span><span>7 novembre 2017</span><span>.</span></p><p style="margin-bottom:12pt; margin-top:60pt; text-align:center"><span style="font-size:28pt; font-weight:bold">PROPOSITION DE LOI ORGANIQUE</span></p><p class="aTitrePG" ><a name="B3615926588"><span style="font-family:'Times New Roman Gras'; font-weight:bold; letter-spacing:0pt">limitant</span><span style="font-style:italic; letter-spacing:0pt"> le </span><span style="font-family:'Times New Roman Gras'; font-weight:bold; letter-spacing:0pt">recours</span><span style="font-style:italic; letter-spacing:0pt"> aux </span><span style="font-family:'Times New Roman Gras'; font-weight:bold; letter-spacing:0pt">dispositions</span><span style="font-style:italic; letter-spacing:0pt"> </span><span style="font-family:'Times New Roman Gras'; font-weight:bold; letter-spacing:0pt">fiscales</span><span style="font-style:italic; letter-spacing:0pt"> de </span><span style="font-family:'Times New Roman Gras'; font-weight:bold; letter-spacing:0pt">portée</span><span style="font-style:italic; letter-spacing:0pt"> </span><span style="font-family:'Times New Roman Gras'; font-weight:bold; letter-spacing:0pt">rétroactive</span><span style="font-style:italic; letter-spacing:0pt">,</span></a><a name="D_limitant_le_recours_aux_dispositions_f"></a></p><p style="margin:30pt -14.25pt 36pt -14.2pt; text-align:center"><span style="font-size:10pt">(Renvoyée à la commission des </span><span style="font-size:10pt">lois constitutionnelles, de la législation et de l</span><span style="font-size:10pt">’</span><span style="font-size:10pt">administration générale</span><br /><span style="font-size:10pt">de la République</span><span style="font-size:10pt">, à défaut de constitution</span><span style="font-size:10pt"> </span><span style="font-size:10pt">d</span><span style="font-size:10pt">’</span><span style="font-size:10pt">une commission spéciale </span><br /><span style="font-size:10pt">dans les délais prévus par les articles 30 et 31 du Règlement.)</span></p><p style="margin-bottom:12pt; margin-top:12pt; text-align:center"><span>présentée</span><span> par</span></p><p style="margin-bottom:12pt; margin-top:12pt; text-align:center"><span style="font-size:13pt">M. Charles de</span><span style="font-size:13pt">&#xa0;</span><span style="font-size:13pt">COURSON,</span></p><p style="margin-top:6pt; text-align:center"><span style="font-size:13pt">député</span><span style="font-size:13pt">.</span></p><p style="margin-top:6pt; text-align:center"><span style="font-size:13pt; font-weight:bold">&#xa0;</span></p></div><br style="clear:both; mso-break-type:section-break; page-break-before:always" /><div><p class="a9ArticleNum" ><a name="B3379874629"><span>Article </span><span>1</span><span style="font-size:9.33pt; vertical-align:super">er</span></a><a name="D_Article_1er"></a></p><p class="aLoiTexte"><span>Les dispositions relatives à l</span><span>’</span><span>assiette, au taux et aux modalités de recouvrement des impositions de toute nature ne disposent que pour l</span><span>’</span><span>avenir. Les dispositions qui s</span><span>’</span><span>appliquent en matière d</span><span>’</span><span>impôts directs à des périodes d</span><span>’</span><span>imposition déjà closes et, en matière de droits d</span><span>’</span><span>enregistrement, à des opérations déjà réalisées, ont une portée rétroactive. </span><span>À</span><span> titre exceptionnel, des dispositions modifiant l</span><span>’</span><span>assiette, le taux ou les modalités de recouvrement des impositions de toute nature peuvent s</span><span>’</span><span>appliquer de manière rétroactive en considération d</span><span>’</span><span>un motif d</span><span>’</span><span>intérêt général suffisant, et sous réserve d</span><span>’</span><span>avoir épuisé toutes les dispositions prévues au premier alinéa. Les dispositions visant à diminuer l</span><span>’</span><span>assiette ou le taux d</span><span>’</span><span>impôts indirects peuvent s</span><span>’</span><span>appliquer rétroactivement.</span></p><p class="a9ArticleNum" ><a name="B455843049"><span>Article 2</span></a><a name="D_Article_2"></a></p><p class="aLoiTexte"><span>Les contrats dont l</span><span>’</span><span>exécution s</span><span>’</span><span>étend sur plus d</span><span>’</span><span>une année et moins de quinze ans se poursuivent jusqu</span><span>’</span><span>à leur terme sous le régime fiscal en vigueur à la date de leur engagement, sans considération des dispositions modifiant l</span><span>’</span><span>assiette, le taux ou les modalités de recouvrement des impositions de toute nature après cette date, lorsque ces dispositions empêchent leur équilibre financier.</span></p><p class="a9ArticleNum" ><a name="B167458799"><span>Article 3</span></a><a name="D_Article_3"></a></p><p class="aLoiTexte"><span>Les dispositions créant un avantage fiscal pour une durée déterminée, ou jusqu</span><span>’</span><span>à une date déterminée, ne peuvent être modifiées avant le terme prévu, sauf à les rendre moins sévères que les dispositions anciennes.</span></p><p class="a9ArticleNum" ><a name="B1052423029"><span>Article 4</span></a><a name="D_Article_4"></a></p><p class="aLoiTexte"><span>L</span><span>’</span><span>adoption de dispositions fiscales rétroactives dans les conditions prévues à l</span><span>’article</span><span>&#xa0;</span><span>1</span><span style="font-size:9.33pt; vertical-align:super">er</span><span> doit être motivée par un exposé justifiant leur caractère rétroactif et par une évaluation des conséquences financières pour les contribuables. Ces dispositions doivent être assorties de mesures transitoires, d</span><span>’</span><span>accompagnement ou de compensation, lorsqu</span><span>’</span><span>elles ont pour effet d</span><span>’</span><span>empêcher l</span><span>’</span><span>exercice d</span><span>’</span><span>une activité professionnelle ou d</span><span>’une liberté publique.</span></p></div></body></div>