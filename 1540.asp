<?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"><style type="text/css"> { font-family:'Times New Roman'; font-size:14pt; text-align:justify }h1, h2, h3, h4, h5, h6, p { margin:0pt }li { margin-bottom:0pt; margin-top:0pt }h1 { color:#ff00ff; font-size:14pt; font-weight:bold; margin-top:12pt; text-align:center; text-transform:uppercase }h2 { color:#ff00ff; font-size:14pt; font-weight:bold; text-align:center; text-transform:uppercase }h3 { color:#008000; font-size:14pt; font-weight:bold; margin-left:28.35pt; text-align:justify; text-indent:-28.35pt; text-transform:uppercase }h4 { font-size:10pt; font-weight:bold; margin-left:85.05pt; text-align:justify; text-indent:-21.3pt; text-transform:uppercase }h5 { font-size:14pt; font-weight:bold; margin-left:85.05pt; text-align:justify; text-indent:-21.3pt }h6 { font-size:14pt; font-style:italic; font-weight:normal; margin-left:85.05pt; text-align:justify; text-indent:-21.3pt }.aHeading7 { font-size:14pt; font-style:italic; font-weight:bold; margin-left:85.05pt; text-align:justify; text-indent:-21.3pt }.aHeading8 { font-size:14pt; font-weight:bold; margin-left:85.05pt; text-align:justify; text-indent:21.3pt }.aHeading9 { font-size:10pt; font-style:italic; font-weight:normal; margin-left:85.05pt; text-align:justify; text-indent:21.3pt }.a1TomeIntit0 { font-family:Arial; font-size:13pt; font-style:italic; font-weight:bold; page-break-after:avoid; text-align:center }.a1TomeNum0 { font-family:Arial; font-size:13pt; font-style:italic; margin-top:12pt; page-break-after:avoid; text-align:center }.a2PartieIntit0 { font-family:'Arial Narrow'; font-size:13pt; font-style:italic; font-weight:bold; page-break-after:avoid; text-align:center }.a3LivreNum { font-size:13pt; font-style:italic; margin-bottom:12pt; text-align:center; text-indent:0pt }.a4TitreIntit0 { font-size:14pt; font-style:italic; font-weight:bold; margin-bottom:30pt; margin-top:0pt; page-break-after:avoid; text-align:center; text-indent:0pt; text-transform:uppercase }.a4TitreNum0 { color:#000000; font-size:14pt; font-style:italic; margin-top:12pt; text-align:center; text-transform:uppercase }.a5ChapitreIntit0 { font-size:14pt; font-style:italic; font-weight:bold; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a5ChapitreNum { font-size:14pt; font-style:italic; font-variant:small-caps; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a6SectionIntit0 { font-size:14pt; font-style:italic; font-weight:bold; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a6SectionNum0 { font-size:14pt; font-style:italic; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a7Sous-sectionIntit0 { font-size:14pt; font-style:italic; font-weight:normal; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a7Sous-sectionNum { font-size:14pt; font-style:italic; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a8ParagrapheIntit { font-size:14pt; font-style:italic; font-weight:normal; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a8ParagrapheNum { font-size:14pt; font-style:italic; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a9ArticleNum0 { font-size:14pt; font-style:italic; font-weight:bold; margin-bottom:2pt; margin-top:15pt; page-break-after:avoid; text-align:center; text-indent:0pt }.aLoiTexte { font-size:14pt; margin-bottom:12pt; text-align:justify; text-indent:25.5pt }.a1TomeIntit { font-family:Arial; font-size:13pt; font-weight:bold; page-break-after:avoid; text-align:center }.a1TomeNum { font-family:Arial; font-size:13pt; margin-top:12pt; page-break-after:avoid; text-align:center }.a2PartieIntit { font-family:'Arial Narrow'; font-size:13pt; font-weight:bold; page-break-after:avoid; text-align:center }.a2PartieNum { font-family:Arial; font-size:13pt; margin-top:12pt; page-break-after:avoid; text-align:center }.a3LivreIntit { font-family:'Arial Narrow'; font-size:13pt; font-weight:bold; page-break-after:avoid; text-align:center }.a3LivreNum0 { font-family:'Times New Roman'; font-size:13pt; margin-top:12pt; page-break-after:avoid; text-align:center }.a4TitreIntit { font-size:14pt; font-weight:bold; margin-bottom:30pt; margin-top:0pt; page-break-after:avoid; text-align:center; text-indent:0pt; text-transform:uppercase }.a4TitreNum { font-size:14pt; margin-bottom:12pt; margin-top:24pt; page-break-after:avoid; text-align:center; text-indent:0pt; text-transform:uppercase }.a4titreintit1 { font-family:'CG TIMES (WN)'; font-size:14pt; font-weight:bold; margin-bottom:12pt; margin-top:3pt; text-align:center; text-transform:uppercase }.a5ChapitreIntit { font-size:14pt; font-weight:bold; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a5ChapitreNum0 { font-size:14pt; font-variant:small-caps; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a6SectionIntit { font-size:14pt; font-weight:bold; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a6SectionNum { font-size:14pt; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a7Sous-sectionIntit { font-size:14pt; font-weight:normal; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a7Sous-sectionNum0 { font-size:14pt; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a8ParagrapheIntit0 { font-size:14pt; font-weight:normal; margin-bottom:12pt; margin-top:6pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a8ParagrapheNum0 { font-size:14pt; font-variant:normal; font-weight:normal; margin-bottom:6pt; margin-top:18pt; page-break-after:avoid; text-align:center; text-indent:0pt }.a9ArticleNum { font-size:14pt; font-weight:bold; margin-bottom:12pt; margin-top:24pt; page-break-after:avoid; text-align:center; text-indent:0pt }.Textenonpastill { font-size:14pt; margin-bottom:12pt; text-align:justify; text-indent:25.5pt }.aTitrePG { font-family:'Times New Roman Italique'; font-size:14pt; letter-spacing:0.75pt; margin-bottom:6pt; margin-top:6pt; text-align:center }.aBalloonText { font-family:Tahoma; font-size:8pt; text-align:justify }.aBodyTextIndent { font-size:14pt; text-align:justify; text-indent:27pt }.aCaption { font-size:24pt; font-weight:bold; margin-top:126pt; text-align:center; text-transform:uppercase }.aDate { font-size:14pt; text-align:justify }.Dlibr { font-size:14pt; font-style:italic; margin-bottom:12pt; margin-top:24pt; text-align:justify; text-indent:25.5pt }.aFooter { font-size:14pt; text-align:justify }.aFootnoteText { font-size:14pt; text-align:justify; text-indent:25.5pt }.aHeader { font-size:11pt; margin-bottom:12pt; text-align:center }.aNormalWeb { font-size:14pt; margin-bottom:5pt; margin-top:5pt; text-align:left }.aProjet { font-size:14pt; font-style:italic; margin-bottom:0pt; margin-left:-7.1pt; margin-right:-26.7pt; text-align:center }.Prsident { font-size:14pt; font-style:italic; margin-top:18pt; text-align:justify }.SignaturePrsident { font-size:14pt; font-style:italic; margin-top:6pt; text-align:justify }.aTITRE { font-size:14pt; margin-bottom:12pt; margin-top:24pt; text-align:center; text-transform:uppercase }.aTITRE4 { font-family:'CG TIMES (WN)'; font-size:12pt; font-weight:normal; margin-bottom:12pt; margin-left:-7.1pt; margin-right:-26.7pt; text-align:justify; text-indent:-21.3pt; text-transform:uppercase }.aTitle { font-size:14pt; margin-bottom:12pt; margin-left:-7.1pt; margin-right:-26.7pt; text-align:center }.aTitre1 { font-size:18pt; font-weight:bold; margin:24pt -26.7pt 24pt -7.1pt; text-align:center }.aTitre7 { font-size:18pt; font-weight:bold; margin:24pt -26.7pt 24pt -7.1pt; text-align:justify }.aTitre8 { font-size:14pt; margin-bottom:12pt; margin-left:106.35pt; margin-right:93.75pt; text-align:center }.Titre0 { font-size:14pt; margin-bottom:12pt; margin-left:-7.1pt; margin-right:-26.7pt; text-align:center; text-decoration:underline }.aTrait { border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:1.5pt; font-size:14pt; margin-left:-7.1pt; margin-right:-26.7pt; padding-bottom:1pt; text-align:center }.aconstitution { font-size:14pt; letter-spacing:0.75pt; margin-top:6pt; text-align:center }.adroite { font-size:14pt; text-align:right }.enregistr { border-top-color:#000000; border-top-style:double; border-top-width:1.5pt; font-size:11pt; margin-left:-21.3pt; margin-right:-21.3pt; margin-top:6pt; padding-top:5pt; text-align:center }.atreizieme { font-size:10pt; letter-spacing:0.75pt; margin-top:6pt; text-align:center }span.aLoiTexteCar { font-size:14pt }span.aEloiPastille {white-space: normal;float:left;clear:left; font-size:18pt; font-weight:bold }span.aLnom { font-family:'Times New Roman'; font-size:13pt; text-decoration:none; text-transform:uppercase; vertical-align:baseline }span.Lprnom { font-family:'Times New Roman'; font-size:13pt; text-decoration:none; vertical-align:baseline }span.aPastille { color:#000000; font-size:12pt; font-weight:bold; text-decoration:none; vertical-align:baseline }span.aPastilleBis { color:#000000; font-size:12pt; font-weight:bold }span.aEmphasis { font-style:italic }span.aFootnoteReference { vertical-align:super }span.aHyperlink { color:#0000ff; text-decoration:underline }span.aPageNumber { font-family:'Times New Roman' }span.aPlaceholderText { color:#808080 }span.aStrong { font-weight:bold }span.aStyle24ptToutenmajuscule { font-family:'Times New Roman'; font-size:24pt; text-transform:uppercase }  p{margin-left:40px;} td p{margin-left:0px;} p.StylePastille{margin-left:0px;}  </style><div xmlns="http://www.w3.org/1999/xhtml" ><head><title>PROJET DE LOI</title></div><?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"><!--[if gte mso 9]><xml></div><?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"><o:DocumentProperties><o:Title>PROJET DE LOI</o:Title><o:Author>Didier Borié</o:Author><o:LastAuthor>Didier Borié</o:LastAuthor><o:Revision>2</o:Revision><o:TotalTime>0</o:TotalTime><o:LastPrinted>2018-12-18T15:55:00Z</o:LastPrinted><o:Created>2018-12-28T11:12:00Z</o:Created><o:LastSaved>2018-12-28T11:12:00Z</o:LastSaved><o:Pages>5</o:Pages><o:Words>1432</o:Words><o:Characters>7880</o:Characters><o:Company>SENAT</o:Company><o:Lines>65</o:Lines><o:Paragraphs>18</o:Paragraphs><o:CharactersWithSpaces>9294</o:CharactersWithSpaces><o:Version>14.0000</o:Version></o:DocumentProperties></div><?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"></xml><![endif]--></div><?xml version="1.0" encoding="UTF-8"?><meta http-equiv="content-type" content="text/html; charset=UTF-8"></head><body><div><p style="text-align:center"><img src="1540images/temporaryFile.001.png" width="144" height="82" alt="Description : LOGO" /></p><p style="text-align:center"><span style="font-size:28pt">N</span><span style="font-size:28pt; vertical-align:3pt">°</span><span style="font-size:28pt">&#xa0;</span><span style="font-size:28pt">1540</span></p><p style="text-align:center"><span style="font-weight:bold; vertical-align:3pt">_____</span></p><p style="text-align:center"><span class="aStyle24ptToutenmajuscule" style="font-size:28pt">ASSEMBLÉE  NATIONALE</span></p><p class="aconstitution"><span style="letter-spacing:0pt">CONSTITUTION DU 4 OCTOBRE 1958</span></p><p class="atreizieme"><span style="letter-spacing:0pt">QUINZI</span><span style="letter-spacing:0pt">ÈME</span><span style="letter-spacing:0pt"> </span><span style="letter-spacing:0pt">LÉGISLATURE</span></p><p class="enregistr" style="margin-left:0pt; margin-right:0pt"><span>Enregistré à la Présidence de l</span><span>’</span><span>Assemblée nationale le </span><span>19 décembre 2018</span><span>.</span></p><p style="margin-bottom:12pt; margin-top:60pt; text-align:center"><span style="font-size:28pt; font-weight:bold">PROPOSITION DE LOI</span></p><p class="aTitrePG" ><a name="B2783909222"><span>pour</span><span> une </span><span style="font-family:'Times New Roman'; font-weight:bold">école</span><span> </span><span style="font-family:'Times New Roman'; font-weight:bold">vraiment</span><span> </span><span style="font-family:'Times New Roman'; font-weight:bold">inclusive</span><span>,</span></a><a name="D_pour_une_ecole_vraiment_inclusive"></a></p><p style="margin:30pt -14.25pt 36pt -14.2pt; text-align:center"><span style="font-size:10pt">(Renvoyée à la commission des </span><span style="font-size:10pt">affaires culturelles et de l</span><span style="font-size:10pt">’</span><span style="font-size:10pt">éducation</span><span style="font-size:10pt">, à défaut de constitution</span><br /><span style="font-size:10pt">d</span><span style="font-size:10pt">’</span><span style="font-size:10pt">une commission spéciale dans les délais prévus par les articles 30 et 31 du Règlement.)</span></p><p style="margin-bottom:6pt; text-align:center"><span style="font-size:13pt">présentée</span><span style="font-size:13pt"> par Mesdames et Messieurs</span></p><p style="margin-top:6pt"><span class="aLnom" style="text-transform:none">Christophe BOUILLON,</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">Laurence DUMONT,</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">Michèle VICTORY,</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">Jean</span><span class="aLnom" style="text-transform:none">&#x2011;</span><span class="aLnom" style="text-transform:none">Louis </span><span class="aLnom" style="text-transform:none">BRICOUT</span><span class="aLnom" style="text-transform:none">,</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">Régis </span><span class="aLnom" style="text-transform:none">JUANICO</span><span class="aLnom" style="text-transform:none">,</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">Josette MANIN,</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">George PAU</span><span class="aLnom" style="text-transform:none">&#x2011;</span><span class="aLnom" style="text-transform:none">LANGEVIN,</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">Sylvie </span><span class="aLnom" style="text-transform:none">TOLMONT</span><span class="aLnom" style="text-transform:none">,</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">Valérie RABAULT</span><span class="aLnom" style="text-transform:none"> </span><span class="aLnom" style="text-transform:none">et les membres du groupe Socialistes </span><span class="aLnom" style="font-size:8.67pt; text-transform:none; vertical-align:super">(1)</span><span class="aLnom" style="text-transform:none"> et apparentés </span><span class="aLnom" style="font-size:8.67pt; text-transform:none; vertical-align:super">(2)</span><span class="aLnom" style="text-transform:none">,</span></p><p style="margin-top:6pt; text-align:center"><span style="font-size:13pt">députés.</span></p><p style="margin-top:6pt"><span style="font-size:13pt">____________________________________</span></p><p style="margin-top:6pt"><span style="font-size:11pt">(1)</span><span style="font-size:11pt">&#xa0;</span><span style="font-size:11pt; font-style:italic">Ce groupe est composé de Mesdames et Messieurs</span><span style="font-size:11pt">&#xa0;</span><span style="font-size:11pt">:</span><span style="font-size:11pt"> Joël </span><span style="font-size:11pt">Aviragnet</span><span style="font-size:11pt">, </span><span style="font-size:11pt">Ericka</span><span style="font-size:11pt"> </span><span style="font-size:11pt">Bareigts</span><span style="font-size:11pt">, Marie</span><span style="font-size:11pt">&#x2011;</span><span style="font-size:11pt">Noëlle </span><span style="font-size:11pt">Battistel</span><span style="font-size:11pt">, Gisèle </span><span style="font-size:11pt">Biémouret</span><span style="font-size:11pt">, Christophe Bouillon, Jean</span><span style="font-size:11pt">&#x2011;</span><span style="font-size:11pt">Louis </span><span style="font-size:11pt">Bricout</span><span style="font-size:11pt">, Luc </span><span style="font-size:11pt">Carvounas</span><span style="font-size:11pt">, Alain David, Laurence Dumont, Olivier Faure, Guillaume </span><span style="font-size:11pt">Garot</span><span style="font-size:11pt">, David Habib, Marietta </span><span style="font-size:11pt">Karamanli</span><span style="font-size:11pt">, Jérôme Lambert, George Pau</span><span style="font-size:11pt">&#x2011;</span><span style="font-size:11pt">Langevin, Christine Pires Beaune, Dominique Potier, Joaquim Pueyo, Valérie Rabault, Hervé </span><span style="font-size:11pt">Saulignac</span><span style="font-size:11pt">, Sylvie </span><span style="font-size:11pt">Tolmont</span><span style="font-size:11pt">, Cécile Untermaier, Hélène Vainqueur</span><span style="font-size:11pt">&#x2011;</span><span style="font-size:11pt">Christophe, Boris </span><span style="font-size:11pt">Vallaud</span><span style="font-size:11pt">, Michèle Victory.</span></p><p style="margin-top:6pt"><span style="font-size:11pt">(2)</span><span style="font-size:11pt">&#xa0;</span><span style="font-size:11pt">MM. Christian Hutin, Régis </span><span style="font-size:11pt">Juanico</span><span style="font-size:11pt">, Serge </span><span style="font-size:11pt">Letchimy</span><span style="font-size:11pt">, Mme Josette Manin.</span></p><p style="margin-top:6pt; text-align:center"><span style="font-size:13pt; font-weight:bold">&#xa0;</span></p></div><br style="clear:both; mso-break-type:section-break; page-break-before:always" /><div><p class="a9ArticleNum" ><a name="B2718691476"><span>Article </span><span>1</span><span style="font-size:9.33pt; vertical-align:super">er</span></a><a name="D_Article_1er"></a></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(1) </span><span>I.</span><span>&#xa0;</span><span>–</span><span>&#xa0;</span><span>Le premier alinéa de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>112</span><span>&#x2011;</span><span>1 du code de l</span><span>’</span><span>éducation est complété par deux phrases ainsi rédigées</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(2) </span><span>«</span><span>&#xa0;</span><span>Lorsque la commission mentionnée à l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>146</span><span>&#x2011;</span><span>9 du code de l</span><span>’</span><span>action sociale et des familles constate que la scolarisation d</span><span>’</span><span>un enfant ou d</span><span>’</span><span>un adolescent en situation de handicap dans une classe de l</span><span>’</span><span>enseignement public ou d</span><span>’</span><span>un établissement mentionné à l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>442</span><span>&#x2011;</span><span>1 du présent code requiert une aide individuelle ou mutualisée, le service public de l</span><span>’</span><span>éducation, conformément à l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>111</span><span>&#x2011;</span><span>1, assure aux parents ou au représentant légal de cet enfant ou adolescent l</span><span>’</span><span>affectation d</span><span>’</span><span>un accompagnant des élèves en situation de handicap au plus tard le quarante</span><span>&#x2011;</span><span>cinquième jour précédant la rentrée scolaire.</span><span> </span><span>Les parents ou le représentant légal de l</span><span>’</span><span>enfant ou de l</span><span>’</span><span>adolescent en situation de handicap bénéficient d</span><span>’</span><span>un entretien avec le ou les enseignants qui en ont la charge ainsi qu</span><span>’</span><span>avec la personne chargée de l</span><span>’</span><span>aide individuelle ou mutualisée au plus tard le jour ouvré précédant la rentrée scolaire.</span><span>&#xa0;</span><span>»</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(3) </span><span>II.</span><span>&#xa0;</span><span>–</span><span>&#xa0;</span><span>Un décret en Conseil d</span><span>’</span><span>État fixe les modalités d</span><span>’</span><span>application du présent article.</span></p><p class="a9ArticleNum" ><a name="B2388602852"><span>Article 2</span></a><a name="D_Article_2"></a></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(1) </span><span>Les cinquième à septième alinéas de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>917</span><span>&#x2011;</span><span>1 du code de l</span><span>’</span><span>éducation sont remplacés par deux alinéas ainsi rédigés</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(2) </span><span>«</span><span>&#xa0;</span><span>Les accompagnants des élèves en situation de handicap sont recrutés par contrat à durée indéterminée.</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(3) </span><span>«</span><span>&#xa0;</span><span>Ils bénéficient d</span><span>’</span><span>une formation spécifique pour l</span><span>’</span><span>accomplissement de leurs fonctions, mise en œuvre en collaboration avec les associations d</span><span>’</span><span>aide aux familles d</span><span>’</span><span>enfants en situation de handicap. Dans le cadre de l</span><span>’</span><span>accomplissement de leurs fonctions, et selon des modalités déterminées par décret en Conseil d</span><span>’</span><span>État, ils bénéficient de la formation continue et peuvent demander à faire valider l</span><span>’</span><span>expérience acquise dans les conditions définies aux articles L.</span><span>&#xa0;</span><span>6111</span><span>&#x2011;</span><span>1, L.</span><span>&#xa0;</span><span>6311</span><span>&#x2011;</span><span>1, L.</span><span>&#xa0;</span><span>6411</span><span>&#x2011;</span><span>1 et L.</span><span>&#xa0;</span><span>6422</span><span>&#x2011;</span><span>1 du code du travail, en vue de l</span><span>’</span><span>obtention d</span><span>’</span><span>un diplôme national ou d</span><span>’</span><span>un titre professionnel enregistré et classé au niveau</span><span>&#xa0;</span><span>IV ou au niveau</span><span>&#xa0;</span><span>III du répertoire national des certifications professionnelles.</span><span>&#xa0;</span><span>»</span></p><p class="a9ArticleNum" ><a name="B2519349726"><span>Article 3</span></a><a name="D_Article_3"></a></p><p class="aLoiTexte"><span>Le deuxième alinéa de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>112</span><span>&#x2011;</span><span>2</span><span>&#x2011;</span><span>1 du code de l</span><span>’</span><span>éducation est complété par les mots</span><span>&#xa0;</span><span>:</span><span> «</span><span>&#xa0;</span><span>,</span><span>&#xa0;</span><span>ainsi que les personnes chargées de l</span><span>’</span><span>aide individuelle ou mutualisée prescrite par la commission mentionnée à l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>146</span><span>&#x2011;</span><span>9 du code de l</span><span>’</span><span>action sociale et des familles.</span><span>&#xa0;</span><span>»</span></p><p class="a9ArticleNum" ><a name="B1825708074"><span>Article 4</span></a><a name="D_Article_4"></a></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(1) </span><span>L</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>112</span><span>&#x2011;</span><span>5 du code de l</span><span>’</span><span>éducation est complété par un alinéa ainsi rédigé</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(2) </span><span>«</span><span>&#xa0;</span><span>Un décret en Conseil d</span><span>’</span><span>État précise le volume horaire et le cahier des charges des contenus de la formation spécifique mentionnée au premier alinéa.</span><span>&#xa0;</span><span>»</span></p><p class="a9ArticleNum" ><a name="B2393841667"><span>Article 5</span></a><a name="D_Article_5"></a></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(1) </span><span>Après </span><span>le premier </span><span>alinéa</span><span> </span><span>de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>351</span><span>&#x2011;</span><span>1 du code de l</span><span>’</span><span>éducation, sont insérés deux alinéas ainsi rédigés</span><span>&#xa0;</span><span>:</span><span> </span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(2) </span><span>«</span><span>&#xa0;</span><span>Le nombre total des élèves scolarisés au sein d</span><span>’</span><span>une classe des écoles maternelles et élémentaires ou des établissements d</span><span>’</span><span>enseignement privés du premier degré sous contrat d</span><span>’</span><span>association ne peut être supérieur à vingt lorsqu</span><span>’</span><span>est scolarisé, au sein de cette classe, au moins un enfant ou adolescent présentant un handicap ou un trouble de santé invalidant et bénéficiant à ce titre d</span><span>’</span><span>un projet personnalisé de scolarisation.</span><span>&#xa0;</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(3) </span><span>«</span><span>&#xa0;</span><span>La dotation horaire globalisée allouée aux établissements mentionnés aux articles L.</span><span>&#xa0;</span><span>213</span><span>&#x2011;</span><span>2, L.</span><span>&#xa0;</span><span>214</span><span>&#x2011;</span><span>6, L.</span><span>&#xa0;</span><span>422</span><span>&#x2011;</span><span>1, L.</span><span>&#xa0;</span><span>422</span><span>&#x2011;</span><span>2 et L.</span><span>&#xa0;</span><span>442</span><span>&#x2011;</span><span>1 du présent code et aux articles L.</span><span>&#xa0;</span><span>811</span><span>&#x2011;</span><span>8 et L.</span><span>&#xa0;</span><span>813</span><span>&#x2011;</span><span>1 du code rural et de la pêche maritime, fait l</span><span>’</span><span>objet d</span><span>’</span><span>une bonification proportionnelle au nombre d</span><span>’</span><span>élèves en situation de handicap bénéficiaires d</span><span>’</span><span>un projet personnalisé de scolarisation qui sont scolarisés dans ces établissements, selon des modalités déterminées par décret en Conseil d</span><span>’</span><span>État.</span><span>&#xa0;</span><span>»  </span></p><p class="a9ArticleNum" ><a name="B2748912021"><span>Article 6</span></a><a name="D_Article_6"></a></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(1) </span><span>L</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>351</span><span>&#x2011;</span><span>3 du code de l</span><span>’</span><span>éducation est ainsi modifié</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(2) </span><span>1°</span><span>&#xa0;</span><span>La première phrase du deuxième alinéa est complétée par les mots</span><span>&#xa0;</span><span>:</span><span> </span><span>«</span><span>&#xa0;</span><span>et en détermine la quotité horaire minimale.</span><span>&#xa0;</span><span>»</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(3) </span><span>2°</span><span>&#xa0;</span><span>Est ajouté un alinéa ainsi rédigé</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(4) </span><span>«</span><span>&#xa0;</span><span>Quelle que soit la nature de l</span><span>’</span><span>aide que la scolarisation de l</span><span>’</span><span>enfant ou de l</span><span>’</span><span>adolescent requiert, cette aide lui est apportée dès le premier jour de sa scolarité.</span><span> </span><span>Il est donné récépissé d</span><span>’</span><span>une demande d</span><span>’</span><span>aide formulée auprès d</span><span>’</span><span>une maison départementale des personnes handicapées dans un délai au plus égal à quinze jours à compter de la date de son dépôt. Ce récépissé indique si le dossier de demande d</span><span>’</span><span>aide est complet ou incomplet. Le cas échéant, une fois les pièces requises reçues, un récépissé indiquant que le dossier est complet est immédiatement délivré. La demande d</span><span>’</span><span>aide est examinée par la maison départementale des personnes handicapées dans un délai qui est au plus égal à deux mois à compter de la date de la délivrance du récépissé indiquant que le dossier est complet, et qui, dans tous les cas, permet une solution dès le premier jour de scolarité de l</span><span>’</span><span>enfant.</span><span>&#xa0;</span><span>»</span></p><p class="a9ArticleNum" ><a name="B2706046520"><span>Article 7</span></a><a name="D_Article_7"></a></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(1) </span><span>I.</span><span>&#xa0;</span><span>–</span><span>&#xa0;</span><span>Le code de l</span><span>’</span><span>éducation est ainsi modifié</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(2) </span><span>1°</span><span>&#xa0;</span><span>L</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>212</span><span>&#x2011;</span><span>4 est complété par une phrase ainsi rédigée</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(3) </span><span>«</span><span>&#xa0;</span><span>Lorsque la construction d</span><span>’</span><span>une école maternelle ou élémentaire d</span><span>’</span><span>enseignement public a été décidée après la promulgation de la loi n°</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span> du</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span> </span><span>pour une école vraiment inclusive, le conseil municipal recueille, sur le projet de construction, l</span><span>’</span><span>avis d</span><span>’</span><span>un établissement ou service mentionné aux</span><span>&#xa0;</span><span>2° et</span><span>&#xa0;</span><span>3° du</span><span>&#xa0;</span><span>I de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>312</span><span>&#x2011;</span><span>1 du code de l</span><span>’</span><span>action sociale et des familles.</span><span>&#xa0;</span><span>»</span><span>&#xa0;</span><span>;</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(4) </span><span>2°</span><span>&#xa0;</span><span>Après la deuxième phrase </span><span>du premier </span><span>alinéa de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>213</span><span>&#x2011;</span><span>2, est inséré</span><span>e</span><span> une phrase ainsi rédigée</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(5) </span><span>«</span><span>&#xa0;</span><span>Lorsque la construction d</span><span>’</span><span>un collège d</span><span>’</span><span>enseignement public a été décidée après la promulgation de la loi n°</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span> du</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span> pour une école vraiment inclusive, le conseil départemental recueille, sur le projet de construction, l</span><span>’</span><span>avis d</span><span>’</span><span>un établissement ou service mentionné aux</span><span>&#xa0;</span><span>2° et</span><span>&#xa0;</span><span>3° du I de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>312</span><span>&#x2011;</span><span>1 du code de l</span><span>’</span><span>action sociale et des familles.</span><span>&#xa0;</span><span>»</span><span>&#xa0;</span><span>;</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(6) </span><span>3°</span><span>&#xa0;</span><span>Après la deuxième phrase du premier </span><span>alinéa </span><span>de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>214</span><span>&#x2011;</span><span>6, est inséré</span><span>e</span><span> une phrase ainsi rédigée</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(7) </span><span>«</span><span>&#xa0;</span><span>Lorsque la construction d</span><span>’</span><span>un lycée d</span><span>’</span><span>enseignement public a été décidée après la promulgation de la loi n°</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span> du</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span> </span><span>pour une école vraiment inclusive, le conseil régional recueille, sur le projet de </span><span>construction, l</span><span>’</span><span>avis d</span><span>’</span><span>un établis</span><span>sement ou service mentionné aux</span><span>&#xa0;</span><span>2° et</span><span>&#xa0;</span><span>3° du</span><span>&#xa0;</span><span>I de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>312</span><span>&#x2011;</span><span>1 du code de l</span><span>’</span><span>action sociale et des familles.</span><span>&#xa0;</span><span>»</span><span>&#xa0;</span><span>;</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(8) </span><span>II.</span><span>&#xa0;</span><span>–</span><span>&#xa0;</span><span>Le cinquième alinéa de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>4424</span><span>&#x2011;</span><span>1 du code général des collectivités territoriales est complété par une phrase ainsi rédigée</span><span>&#xa0;</span><span>:</span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(9) </span><span>«</span><span>&#xa0;</span><span>Lorsque la construction des établissements précités a été décidée après la promulgation de la loi n°</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span> du</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span>&#xa0;</span><span> pour une école vraiment inclusive, la collectivité territoriale de Corse recueille, sur le projet de construction, l</span><span>’</span><span>avis d</span><span>’</span><span>un établissement ou service mentionné aux</span><span>&#xa0;</span><span>2° et</span><span>&#xa0;</span><span>3° du</span><span>&#xa0;</span><span>I de l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>312</span><span>&#x2011;</span><span>1 du code de l</span><span>’</span><span>action sociale et des familles.</span><span>&#xa0;</span><span>».</span></p><p class="a9ArticleNum" ><a name="B3499598569"><span>Article 8</span></a><a name="D_Article_8"></a></p><p class="aLoiTexte"><span>Dans un délai de six mois à compter de la promulgation de la présente loi, le Gouvernement remet au Parlement un rapport sur les moyens de connaissance statistique de la situation et des besoins des personnes en situation de handicap, notamment des enfants et adolescents en situation de handicap, sur les moyens d</span><span>’</span><span>évaluation des politiques publiques menées en France dans ce domaine, et en particulier sur l</span><span>’</span><span>effectivité et les délais de la mise en œuvre des décisions d</span><span>’</span><span>attribution d</span><span>’</span><span>un accompagnement humain individuel ou mutualisé qui sont prises par les commissions mentionnées à l</span><span>’</span><span>article L.</span><span>&#xa0;</span><span>146</span><span>&#x2011;</span><span>9 du code de l</span><span>’</span><span>action sociale et des familles ainsi que sur le niveau scolaire, la formation professionnelle et l</span><span>’</span><span>inclusion sociale des jeunes en situation de handicap.</span></p><p class="a9ArticleNum" ><a name="B3378263313"><span>Article 9</span></a><a name="D_Article_9"></a></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(1) </span><span>La </span><span>charge pour </span><span>l</span><span>’</span><span>État est compensée par la majoration à due concurrence du taux du</span><span>&#xa0;</span><span>1° du</span><span>&#xa0;</span><span>B du</span><span>&#xa0;</span><span>1 de l</span><span>’</span><span>article</span><span>&#xa0;</span><span>200</span><span>&#xa0;</span><span>A du code général des impôts. </span></p><p class="aLoiTexte"><span class="aEloiPastille" style="font-size:14pt">(2) </span><span>La charge pour les collectivités territoriales est compensée à due concurrence par la majoration de la dotation globale de fonctionnement et, corrélativement, pour l</span><span>’</span><span>État, par la majoration du taux du</span><span>&#xa0;</span><span>1</span><span>° du</span><span>&#xa0;</span><span>B du</span><span>&#xa0;</span><span>1</span><span> </span><span>de l</span><span>’</span><span>article</span><span>&#xa0;</span><span>200</span><span>&#xa0;</span><span>A du code général des impôts. </span></p></div></body></div>